from rest_framework import serializers

from . import models


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Message
        fields = ['message', 'show_from', 'show_until', 'background_colour', 'foreground_colour']

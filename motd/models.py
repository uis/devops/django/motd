import datetime
import textwrap

from django.conf import settings
from django.db import models
from django.utils import timezone


def _default_motd_show_from():
    return timezone.now()


def _default_motd_show_until():
    return _default_motd_show_from() + datetime.timedelta(seconds=settings.MOTD_DEFAULT_LIFETIME)


class Message(models.Model):
    """
    A message show to the user in the UI.

    """
    message = models.TextField(
        blank=False, help_text='Message to show user. Markdown formatting is supported.')

    show_from = models.DateTimeField(
        default=_default_motd_show_from, blank=True,
        help_text='Time and date before which message should not be shown')

    show_until = models.DateTimeField(
        default=_default_motd_show_until, blank=True,
        help_text='Time and date after which message should not be shown')

    background_colour = models.CharField(
        max_length=255, default='#e0e0e0',
        help_text='Background colour in CSS form')

    foreground_colour = models.CharField(
        max_length=255, default='#212121',
        help_text='Foreground colour in CSS form')

    class Meta:
        indexes = [
            # The most common query is for messages which should currently be shown
            models.Index(fields=['show_from', 'show_until']),
        ]

    def __str__(self):
        short_message = textwrap.shorten(self.message, width=15, placeholder='...')
        return f'"{short_message}" from {self.show_from:%c} to {self.show_until:%c}'

import datetime

from django.test import TestCase
from django.utils import timezone
from rest_framework.test import APIRequestFactory

from .. import models
from .. import views


class TestMessageListViewSet(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()

    def test_no_messages(self):
        """
        With no messages in the db, we should get no results

        """
        response = self.get()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['count'], 0)
        self.assertEqual(response.data['results'], [])

    def test_basic_usage(self):
        """
        A message appears in the API response.

        """
        m1 = models.Message.objects.create(message="hello")
        response = self.get()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['count'], 1)
        resp_m1 = response.data['results'][0]
        self.assertEqual(resp_m1['message'], m1.message)

    def test_expired_messages(self):
        """
        A message from the past does not appear.

        """
        m1 = models.Message.objects.create(message="hello")
        response = self.get()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['count'], 1)

        m1.show_from -= datetime.timedelta(days=10)
        m1.show_until -= datetime.timedelta(days=10)
        m1.save()

        response = self.get()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['count'], 0)

    def test_future_messages(self):
        """
        A message from the future does not appear.

        """
        m1 = models.Message.objects.create(message="hello")
        response = self.get()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['count'], 1)

        m1.show_from = timezone.now() + datetime.timedelta(days=10)
        m1.show_until = m1.show_from + datetime.timedelta(days=10)
        m1.save()

        response = self.get()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['count'], 0)

    def get(self):
        """Perform a GET request to the viewset and return the response."""
        request = self.factory.get('/')
        return views.MessageViewSet.as_view(actions={'get': 'list'})(request)

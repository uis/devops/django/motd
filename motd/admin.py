import textwrap

from django.contrib import admin
from django.forms import ModelForm
from django.forms.widgets import TextInput

from . import models


class MessageForm(ModelForm):
    class Meta:
        model = models.Message
        fields = ['message', 'background_colour', 'foreground_colour', 'show_from', 'show_until']
        widgets = {
            'foreground_colour': TextInput(attrs={'type': 'color'}),
            'background_colour': TextInput(attrs={'type': 'color'}),
        }


@admin.register(models.Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ['short_message', 'show_from', 'show_until']
    ordering = ['-show_from']
    form = MessageForm

    def short_message(self, obj):
        return textwrap.shorten(obj.message, width=80, placeholder='...')
    short_message.short_description = 'message'

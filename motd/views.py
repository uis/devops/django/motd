"""
API views for MotD

"""
from django.utils import timezone
from rest_framework import mixins, pagination, viewsets

from . import models
from . import serializers


class Pagination(pagination.PageNumberPagination):
    page_size = 100
    page_size_query_param = 'page_size'
    max_page_size = 500


class MessageViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    queryset = models.Message.objects
    serializer_class = serializers.MessageSerializer
    pagination_class = Pagination

    def get_queryset(self):
        now = timezone.now()
        return (
            super().get_queryset()
            .order_by('show_from')
            .filter(show_from__lt=now, show_until__gt=now)
        )

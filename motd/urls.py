from rest_framework import routers

import motd.views

router = routers.DefaultRouter()
router.register(r'', motd.views.MessageViewSet)

urlpatterns = router.urls

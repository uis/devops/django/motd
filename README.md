# Introduction

MOTD is a Django app that displays the message of the day. This message
is specified by the user and the time period the message should be
displayed for can be edited.

## Use

Install motd using pip:

```bash
pip install git+https://gitlab.developers.cam.ac.uk/uis/devops/django/motd.git
```

Add motd to the installed applications in the project configuration:

```python
    INSTALLED_APPS=(
    ...
        'motd',
    ...
    ),
```

and in your project's urls.py file:
```python
    urlpatterns = [
       ...
       path('motd/', include('motd.urls')),
       ...
    ]
```python

Run `python manage.py migrate` to create the motd models.

Start the development server and visit the [Admin Page](http://127.0.0.1:8000/admin/)
to create a message.

This package can be used by the UI in a component such as[BannerMessages.tsx]
(https://gitlab.developers.cam.ac.uk/uis/devops/uga/smi/blob/master/ui/frontend/src/components/BannerMessages.tsx),
which will display the MOTD.

Visit the [Main Page](http://127.0.0.1:8000/) to view the message.
